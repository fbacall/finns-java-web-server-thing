package webServer;
import java.io.*;
import java.net.*;

public class WebServer {

    private ServerSocket socket;
    private static final int CLIENT_TIMEOUT = 3 * 60 * 1000;
    private String rootDir;

    public static void main (String [] args) throws IOException {
        ServerSocket socket = null;
        int port = Integer.parseInt(args[0]);        
        try {
            socket = new ServerSocket(port);
        }
        catch(IOException e) {
            System.err.println("Couldn't create server socket on port: " + port);
            System.exit(-1);
        }

        WebServer server = new WebServer(socket);
        server.setRootDir(args[1]);
        System.out.println("Server running on port: " + port);

        server.serve();
    }

    public WebServer(ServerSocket socket) {
        this.socket = socket;
    }

    public void serve() throws IOException {
        while(true) {
            Socket newUserSocket = socket.accept();
            newUserSocket.setSoTimeout(CLIENT_TIMEOUT);
            new WebServerThread(newUserSocket, this).start();
        }
    }

    public String getRootDir() {
        return rootDir;
    }

    public void setRootDir(String rootDir) {
        this.rootDir = rootDir;
    }
}
