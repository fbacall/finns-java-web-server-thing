package webServer;

import java.io.*;
import java.net.*;

public class WebServerThread extends Thread {
    
    public static final String CODE_200 = "200 OK";
    public static final String CODE_404 = "404 Not Found";
    public static final String CODE_500 = "500 Internal Server Error";

    private Socket socket;
    private WebServer server;
    private BufferedReader in;
    private OutputStream rawOut;
    private PrintWriter out;
    public boolean disconnected;

    public WebServerThread(Socket socket, WebServer server) {
        super("WebServerThread");
        this.socket = socket;
        this.server = server;
    }

    public void run() {
        try {
            // New client msg
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            rawOut = socket.getOutputStream();
            out = new PrintWriter(rawOut, true);

            System.out.println("New connection (" + socket.getInetAddress() + ")");
            
            String msg = null;
            try {
                while ((msg = in.readLine()) != null && !msg.equals("")) { 
                    System.out.println(msg);
                    String [] parts = msg.split(" ");
                    if(parts[0].equals("GET")) {
                        if(parts[1].equals("/"))
                            parts[1] = "/index.html";
                        String filePath = URLDecoder.decode(server.getRootDir() + parts[1].replaceAll("\\.\\./",""), "UTF-8");
                        System.out.println("Serving: " + filePath);
                        File file = new File(filePath);
                        if(file.canRead())
                            sendResponse(CODE_200, file);
                        else
                            sendResponse(CODE_404);
                    }
                }
            }
            catch (SocketTimeoutException e) {
                System.err.println("Connection timed out ("+ socket.getInetAddress() + ")");
            }
            catch (SocketException e) {
                System.err.println("Socket exception "+ socket.getInetAddress() + ")");
                e.printStackTrace();
            }
            finally {
                out.close();
                in.close();
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Send a file response.
    private void sendResponse(String code, File f) throws IOException {
        byte[] buffer;
        try {
            buffer = readFile(f);
        } 
        catch(IOException e) {
            sendResponse(CODE_500);
            return;
        }
       
        out.println("HTTP/1.1 " + code);
        out.println("Content-Type: " + getContentType(f.getName()));
        out.println("Content-Length: " + f.length());
        out.println();
        rawOut.write(buffer);
    }    
    
    // Plain text response
    private void sendResponse(String code, String body) {
        out.println("HTTP/1.1 " + code);
        out.println("Content-Type: text/plain");
        out.println("Content-Length: " + body.length());
        out.println();
        out.println(body);
    }
    
    // Send generic response according to the code. 
    // Will either send an html file or send a plain text response if no file exists
    private void sendResponse(String code) throws IOException {
        String message = null;
        File file = null;
        if(code == CODE_404) {
            file = new File(server.getRootDir() + "/404.html");
            message = "404 - Not Found";
        }
        else if(code == CODE_500) {
            file = new File(server.getRootDir() + "/500.html");
            message = "500 - Internal Server Error";
        }
        
        if(file != null && message != null) {
            if(file.canRead()) {
                sendResponse(code, file);
            }
            else {
                sendResponse(code, message);
            }
        }
    }
    
    private byte[] readFile(File f) throws IOException, FileNotFoundException {
        FileInputStream fileIn = new FileInputStream(f); 
        byte[] buffer = new byte[(int)f.length()]; 
        int i = 0;
        int c; 
        while ((c = fileIn.read()) != -1) {
            buffer[i++] = (byte) c;
        }        
        fileIn.close();        
        return buffer;
    }
    
    private static String getContentType(String fileName) {
        String [] fileNameComponents = fileName.split("\\.");
        String ext = fileNameComponents[fileNameComponents.length-1];  
        String contentType;
        if(ext.equals("html") || ext.equals("css") || ext.equals("xml") || ext.equals("xsl")) {
            contentType = "text/" + ext;
        }
        else if(ext.equals("gif") || ext.equals("jpg") || ext.equals("jpeg") || ext.equals("png")) {
            contentType = "image/" + ext;
        }
        else if(ext.equals("js")) {
            contentType = "application/x-javascript";
        }
        else {
            contentType = "text/plain";
        }
        return contentType;
    }
}
